const mongoose = require("mongoose");

const app = require("./src/app");
const dbConfig = require("./src/config/db.config");

// connect to mongodb
mongoose.connect(`mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
}).then(() => {
    console.log("Successfully connect to MongoDB.");
}).catch(err => {
    console.error("Connection error", err);
    process.exit();
});

// set port, listen for request
const PORT = process.env.PORT || 8080;
let server = app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});

// handle process 'uncaughtException' and 'unhandledRejection' error
const exitHandler = () => {
    if (server) {
        server.close(() => {
            // logger.info('Server closed');
            console.log("Server closed!")
            process.exit(1);
        });
    } else {
        process.exit(1);
    }
};
  
const unexpectedErrorHandler = (err) => {
    // logger.error(err);
    console.log(err);
    exitHandler();
};
  
process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);