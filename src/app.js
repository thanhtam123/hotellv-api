const express = require("express");
const cors = require("cors");
const httpStatus = require("http-status");
const xss = require('xss-clean');
const mongoSanitize = require('express-mongo-sanitize');

const ApiError = require("./utils/ApiError");
const { errorConverter, errorHandler } = require("./middlewares/error");
const routes = require("./routes");

let app = express();

// parse application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: false }));
 
// parse application/json
app.use(express.json());

// sanitize request data
app.use(xss());
app.use(mongoSanitize());

// enable cors
app.use(cors());
app.options('*', cors());

// config routers
app.use('/api', routes);

// send back a 404 error for any unknown api request
app.use(async (req, res, next) => {
    next(new ApiError(httpStatus.NOT_FOUND, `${httpStatus[httpStatus.NOT_FOUND]} Route.`));
});

// convert error to ApiError
app.use(errorConverter);

// handle error
app.use(errorHandler);

module.exports = app;