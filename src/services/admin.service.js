const { Admin } = require("../models");
const httpStatus = require("http-status");
const ApiError = require("../utils/ApiError");

const createAdmin = async (adminBody) => {
    const admin = await Admin.create(adminBody);
    return admin;
};

// const signin = async () {};

const getAdminById = async (adminId) => {
    const admin = await Admin.findById(adminId);
    if (!admin) {
        throw new ApiError(httpStatus.NOT_FOUND, `Not found admin with id ${adminId}.`);
    }
    return admin;
};

const updateProfileById = async (adminId, updateBody) => {
    const admin = await getAdminById(adminId);
    Object.assign(admin, updateBody);
    await admin.save();
    return admin;
};

const changePassword = async (adminId, newPassword) => {
    const admin = await updateProfileById(adminId, { password: newPassword });
    return admin;
};

module.exports = {
    createAdmin,
    // signin,
    getAdminById,
    updateProfileById,
    changePassword,
}