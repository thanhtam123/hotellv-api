const Joi = require("joi").extend(require('@joi/date'));
const { objectId, password, phone } = require("./custom/custom.validation");

const createAdmin = {
    body: Joi.object().keys({
        email: Joi.string().required().email(),
        password: Joi.string().required().custom(password),
        name: Joi.string().required(),
        birthday: Joi.date().format('DD-MM-YYYY').utc(),
        phone: Joi.string().required().custom(phone),
        address: Joi.string().required(),
    }),
};

const getAdminById =  {
    params: Joi.object().keys({
        id: Joi.string().custom(objectId),
    }),
};

const updateAdminById = {
    params: Joi.object().keys({
        id: Joi.string().custom(objectId),
    }),
    body: Joi.object().keys({
        email: Joi.string().email(),
        password: Joi.string().custom(password),
        name: Joi.string(),
        birthDay: Joi.string(),
        phone: Joi.string().custom(phone),
        address: Joi.string(),
    }).min(1),
};

// const changePassword = {

// }

module.exports = {
    createAdmin,
    getAdminById,
    updateAdminById,
    // changePassword,
}