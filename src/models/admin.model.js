const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const validator = require("validator");

const {accountStatus, accountStatusArr} = require("../customs/db/accStatus");

const adminSchema = new mongoose.Schema(
    {
        email: {
            type: String,
            required: true,
            trim: true,
            unique: true,
            validate(value) {
                if (!validator.isEmail(value)) {
                    throw new Error("Invalid email!");
                }
            },
        },
        password: {
            type: String,
            trim: true,
            required: true,
            default: "aaaa4444",
            validate(value) {
                if (value.length < 8) {
                    throw new Error('Password must be at least 8 characters');
                }

                if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
                    throw new Error('Password must contain at least one letter and one number');
                }
            }
        },
        name: {
            type: String,
            trim: true,
            required: true,
        },
        img: {
            type: String,
            default: "placeholder.jpg",
        },
        birthday: {
            type: Date,
            required: true,
        },
        phone: {
            type: String,
            trim: true,
            required: true,
        },
        address: {
            type: String,
            trim: true,
            required: true
        },
        status: {
            type: String,
            trim: true,
            enum: accountStatusArr,
            default: accountStatus.AVAILABLE,
            validate(value) {
                if (!accountStatusArr.includes(value)) {
                    throw new Error(`Invalid account status '${value}'.`);
                }
            }
        }
    },
    {
        timestamps: true,
    }
);

/**
 * pre save hook
 */
// adminSchema.pre('save', async function () {
//     const user = this;

//     //hash password
//     if (user.isModified('password')) {
//         user.password = await bcrypt.hash(user.password, 8);
//     }
// });

/**
 * check if input password is match with user's password
 * @param {string} password
 * @returns {Promise<boolean>}
 */
 adminSchema.methods.isPasswordMatch = async function (password) {
    const user = this;
    // return await bcrypt.compare(password, user.password);
    return password == user.password;
};


const Admin = mongoose.model(
    "Admin",
    adminSchema
);

module.exports = Admin;