const { adminService } = require("../services");
const httpStatus = require("http-status");
const ApiError = require("../utils/ApiError");
const catchAsync = require("../utils/catchAsync");

module.exports.createAdmin = catchAsync(async (req, res, next) => {
    const admin = await adminService.createAdmin(req.body);
    res.status(httpStatus.CREATED).send(admin);
});

// module.exports.signin = catchAsync(async (req, res, next) => {

// });

module.exports.getAdminById = catchAsync(async (req, res, next) => {
    const admin = await adminService.getAdminById(req.params.id);
    res.send(admin);
});

module.exports.updateProfile = catchAsync(async (req, res, next) => {
    const admin = await adminService.updateProfileById(req.params.id, req.body);
    res.send(admin);
});