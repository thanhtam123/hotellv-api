const express = require("express");
const httpStatus = require("http-status");
const routes = express.Router();

routes.get('/', async (req, res) => {
    res.status(httpStatus.OK).send({ message: "Welcome to hotelLV api!"});
});

const defaultRoutes = [
    {
        path: '/admin',
        route: require("./admin.route"),
    }
];

defaultRoutes.forEach(route => {
    routes.use(route.path, route.route);
});

module.exports = routes;