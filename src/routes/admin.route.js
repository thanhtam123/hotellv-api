const express = require("express");
const validate = require("../middlewares/validate");

const { adminController } = require("../controllers");
const { adminValidation } = require("../validations");

const router = express.Router();

router.route("/create")
    .post(validate(adminValidation.createAdmin), adminController.createAdmin);

router.route("/:id")
    .get(validate(adminValidation.getAdminById), adminController.getAdminById)
    .put(validate(adminValidation.updateAdminById), adminController.updateProfile)

// router.route("/:id/change-password")
//     .put(adminController)

module.exports = router;